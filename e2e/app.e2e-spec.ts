import { MajlisCMSPage } from './app.po';

describe('majlis-cms App', () => {
  let page: MajlisCMSPage;

  beforeEach(() => {
    page = new MajlisCMSPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
