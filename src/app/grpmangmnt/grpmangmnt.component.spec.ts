import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrpmangmntComponent } from './grpmangmnt.component';

describe('GrpmangmntComponent', () => {
  let component: GrpmangmntComponent;
  let fixture: ComponentFixture<GrpmangmntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrpmangmntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrpmangmntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
