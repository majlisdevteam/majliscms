import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  
  btnClick = function(){
    console.log('User Mgt')
    // this.router.navigateByUrl('/usermgt');
    this.router.navigate(['./usermgt']);
  }
  
  btnGrp = function(){
    console.log('Grp Mgt')
    this.router.navigate(['./grpmngmnt'])
  }

  btnCat = function(){
    console.log('Cat Mgt')
    this.router.navigate(['./catgrymngmnt'])
  }

  btnEvt = function(){
    console.log('Evt Mgt')
    this.router.navigate(['./evntmngmnt'])
  }

  btnTmplt = function(){
    console.log('Tmplte Mgt')
    this.router.navigate(['./templatemngmnt'])
  }
}
