import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { AlertModule, ModalModule, DatepickerModule } from 'ng2-bootstrap'
import { AppComponent } from './app.component';
import { Observable } from "rxjs/Rx";
import { DashboardComponent } from './dashboard/dashboard.component';
// import {TooltipModule, DatepickerModule} from 'ng2-bootstrap/ng2-bootstrap';
// import { DataTablesModule } from 'angular-datatables';
import { DataTableModule} from "ng2-data-table"

import {routing} from './app.routing';
import { LoginComponent } from './login/login.component';
import { PagetopComponent } from './pagetop/pagetop.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { GrpmangmntComponent } from './grpmangmnt/grpmangmnt.component';
import { CategorymangmntComponent } from './categorymangmnt/categorymangmnt.component';
import { EventmangmntComponent } from './eventmangmnt/eventmangmnt.component';
import { TemplatemangmntComponent } from './templatemangmnt/templatemangmnt.component';
import { UserpromptComponent } from './userprompt/userprompt.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
// import {DatePickerModule} from 'ng2-datepicker-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    PagetopComponent,
    SideBarComponent,
    UsermanagementComponent,
    GrpmangmntComponent,
    CategorymangmntComponent,
    EventmangmntComponent,
     TemplatemangmntComponent,
     UserpromptComponent
     
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    AlertModule.forRoot(),
    DataTableModule,
    ModalModule.forRoot(),
    DatepickerModule.forRoot(),
    PaginationModule.forRoot()
    // DatePickerModule
    // TooltipModule, DatepickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

constructor(private http: Http){

 Observable.interval(15000).subscribe(response=>{
  //  this.sessionCheck();
 });


}


sessionCheck() : any{

   this.http.get("./session.json").subscribe(response=>{

  },error=>{
    console.log("error..")
  })
}


}

 export const AppParams = Object.freeze({
   BASE_PATH : "http://192.0.0.48:8080/APIGateway-1.0/"
 })
