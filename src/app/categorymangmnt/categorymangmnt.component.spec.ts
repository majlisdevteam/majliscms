import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorymangmntComponent } from './categorymangmnt.component';

describe('CategorymangmntComponent', () => {
  let component: CategorymangmntComponent;
  let fixture: ComponentFixture<CategorymangmntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategorymangmntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorymangmntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
