import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplatemangmntComponent } from './templatemangmnt.component';

describe('TemplatemangmntComponent', () => {
  let component: TemplatemangmntComponent;
  let fixture: ComponentFixture<TemplatemangmntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplatemangmntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplatemangmntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
