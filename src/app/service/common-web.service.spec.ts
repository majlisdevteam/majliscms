import { TestBed, inject } from '@angular/core/testing';

import { CommonWebService } from './common-web.service';

describe('CommonWebService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommonWebService]
    });
  });

  it('should ...', inject([CommonWebService], (service: CommonWebService) => {
    expect(service).toBeTruthy();
  }));
});
