import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router'

import { DashboardComponent } from './dashboard/dashboard.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { GrpmangmntComponent } from './grpmangmnt/grpmangmnt.component';
import { CategorymangmntComponent } from './categorymangmnt/categorymangmnt.component';
import { EventmangmntComponent } from './eventmangmnt/eventmangmnt.component';
import { TemplatemangmntComponent } from './templatemangmnt/templatemangmnt.component';

const appRoutes: Routes = [
    {
        path:'dash',
        component:DashboardComponent
    },
    {
        path:'',
        component:LoginComponent
    },
    {
        path:'usermgt',
        component:UsermanagementComponent
    },
    {
        path:'grpmngmnt',
        component:GrpmangmntComponent
    },
    {
        path:'catgrymngmnt',
        component:CategorymangmntComponent
    },
    {
        path:'evntmngmnt',
        component:EventmangmntComponent
    },
    {
        path:'templatemngmnt',
        component:TemplatemangmntComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
